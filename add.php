<?php
	require_once("config.php");
	if(isset($_POST['upload']) && isset($_FILES['image_upload']))
	{
		$valid_type = array("jpeg","jpg","gif","png");
		$ext = explode(".",$_FILES['image_upload']['name']);
		$extension = end($ext);
		if($_FILES['image_upload']['size'] < 20000 && in_array($extension,$valid_type))
		{
			if(move_uploaded_file($_FILES['image_upload']['tmp_name'],"pics/".$_FILES['image_upload']['name']))
			{
				$sql = "INSERT INTO pictures SET image_name ='".$conn->real_escape_string($_FILES['image_upload']['name'])."'";
				$result = $conn->query($sql);
				if(!$result)die($conn->error);
				echo '<script>alert("Image uploaded!");window.location="index.php";</script>';
			}
		}else{
			echo "Wrong file format!";
		}
	}
	//print_r($_FILES);
?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
	</head>
	<body>
		<form action="" method="post" enctype="multipart/form-data">
			Select Image : <input type="file" name="image_upload"/>
			<input type="submit" name="upload" value="upload"/>
		</form>
	</body>
</html>

